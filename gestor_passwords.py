# Script que funciona como un gestor de contraseñas
# Permite: encriptar y desencriptar el texto, agregar nuevas contraseñas y eliminar otras.

# import required module
import getpass
import hashlib
import pyAesCrypt
from os import stat, remove
# encryption/decryption buffer size - 64K
# with stream-oriented functions, setting buffer size is mandatory
bufferSize = 64 * 1024

# encrypt
def encriptar(archivoEncriptar, password):
    archivoEncriptado = str(archivoEncriptar) + ".aes"
    try:
        with open(archivoEncriptar, "rb") as fIn:
            with open(archivoEncriptado, "wb") as fOut:
                pyAesCrypt.encryptStream(fIn, fOut, password, bufferSize)

        print("\x1b[1;32m" + "Archivo encriptado exitosamente" + "\x1b[0;37m")
        print("Nombre del archivo encriptado: " + str(archivoEncriptado))
        respuesta = str(input("¿Desea eliminar el archivo de texto plano?[s/n]:"))
        if(respuesta == 's' or respuesta == 'S'):
            remove(archivoEncriptar)
    except FileNotFoundError:
        print("\x1b[1;31m" + "[ERROR] Archivo no encontrado" + "\x1b[0;37m")


# decrypt
# al desencriptar solo se muestra en pantalla, no se guarda el archivo desencriptado
def desencriptar(archivoDesencriptar, password):
    try:
        datos = ''
        # get encrypted file size
        encFileSize = stat(archivoDesencriptar).st_size
        with open(archivoDesencriptar, "rb") as fIn:
            try:
                with open("dataout.txt", "wb") as fOut:
                    # decrypt file stream
                    pyAesCrypt.decryptStream(fIn, fOut, password, bufferSize, encFileSize)
                with open("dataout.txt") as fileRead:
                    datos = fileRead.read()
                    #print(datos)
            except ValueError:
                print("\x1b[1;31m" + "[ERROR] Contraseña incorrecta" + "\x1b[0;37m")
            finally:
                # remove output file
                remove("dataout.txt")
        return datos
    except FileNotFoundError:
        print("\x1b[1;31m" + "[ERROR] Archivo no encontrado" + "\x1b[0;37m")

# retrona el hash MD5 de la clave pasada como argumento (tipo str)
def getHashMD5(password):
    hash_passwd = hashlib.md5(password.encode("utf-8"))
    password = hash_passwd.hexdigest()

    return password

# agrega datos al final de un archivo y lo vuelve a encriptar
def agregarDatos(nameArchivo, datosAgregar, password):
    nameArchivo = nameArchivo.replace(".aes", "") # elimina la extension .aes
   # print(nameArchivo)
    with open(nameArchivo, "w") as archivo:
        archivo.write(datosAgregar)
    encriptar(nameArchivo, password)
    print("\x1b[1;32m" + "Datos agregados correctamente" + "\x1b[0;37m")

def inputDatos():
    print("A continuación, ingrese los nuevos datos, para finalizar ingrese '$'")
    nuevosDatos = ''
    while True:
        entrada = str(input())
        if(entrada == "$"):
            break
        nuevosDatos += entrada + '\n'
    #print(nuevosDatos)
    return nuevosDatos

if(__name__ == "__main__"):
    while True:
        print("\x1b[1;36m" + "\x1b[4;36m" + "Seleccione una opción" + "\x1b[0;36m")
        print("\x1b[1;36m" + " 1) " + "\x1b[0;37m" + "Encriptar archivo")
        print("\x1b[1;36m" + " 2) " + "\x1b[0;37m" + "Desencriptar archivo")
        print("\x1b[1;36m" + " 3) " + "\x1b[0;37m" + "Guardar nueva contraseña")

        opcion = str(input())
        if(opcion in ['1', '2', '3']):
            break
        print("\x1b[1;31m" + "[ERROR] Opción incorrecta\n" + "\x1b[0;37m")
    
    if(opcion == '1'):
        nameFile = str(input("Ingrese el nombre del archivo a encriptar:"))
        nameFile = nameFile.strip()
        password1 = str(getpass.getpass(prompt="Ingrese su clave de cifrado:"))
        password2 = str(getpass.getpass(prompt="Vuelva a introducir su clave de cifrado:"))
        if(password1 == password2):
            # se utiliza el hash MD5 de la contrasenia como nueva clave
            password = getHashMD5(password1)
            encriptar(nameFile, password)
            # respuesta = str(input("¿Desea eliminar el archivo de texto plano?[s/n]:"))
            # if(respuesta == 's' or respuesta == 'S'):
            #     remove(nameFile)
        else:
            print("\x1b[1;31m" + "[ERROR] Las contraseñas ingresadas no coinciden" + "\x1b[0;37m")
    elif(opcion == '2'):
        nameFile = str(input("Ingrese el nombre del archivo a desencriptar:"))
        nameFile = nameFile.strip()
        password = str(getpass.getpass(prompt="Ingrese la clave de cifrado:"))
        password = getHashMD5(password)
        datos = desencriptar(nameFile, password)
        print(datos)

    else: #3
        nameFile = str(input("Ingrese el nombre del archivo a modificar:"))
        nameFile = nameFile.strip()
        password = str(getpass.getpass(prompt="Ingrese su clave de cifrado:"))
        password = getHashMD5(password)
        datos = desencriptar(nameFile, password)
        nuevosDatos = inputDatos()
        agregarDatos(nameFile, str(datos) + '\n' + nuevosDatos, password)

